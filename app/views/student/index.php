<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php 
$point_table = $data["point_table"];
if($point_table) { 
?>
<h1>Tabla Puntos</h1>
<table>
    <thead>
        <tr>
            <td>Asignatura</td>
            <td>Cantidad</td>
            <td>Fecha expiración</td>        
        </tr>
    </thead>
    <tbody>
        <tr>
        <?php
        foreach($point_table as $row){
            echo "<td>" . $row["class_name"] . "</td>\n";
            echo "<td>" . $row["plus_quantity"] . "</td>\n";
            echo "<td>" . $row["plus_expiration_date"] . "</td>\n";
        }        
        ?>    
        </tr>
    </tbody>

</table>
<?php } ?>
    
</body>
</html>