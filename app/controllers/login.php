<?php

class Login extends Controller{

    public function index()
    {
        $user_model = $this->model("User");
        $email = $this->get("email");
        $pass = $this->get("pass");
        if($email && $pass)
        {            
            if($user_model->isUserValid($email,$pass))
            {
                $user_data = $user_model->getUserByEmail($email);
                $user_model->startSession();
                $user_model->doLogin($user_data);    
                $this->redirect("home"); 
            }
            else
            {
                $this->view("info/error", array("info" => "Usuario o password incorrecta")); 
            }
        }
        else
        {
            $this->view("home/login");
        }
    }

    public function logout()
    {
        $user_model = $this->model("User");
        $user_model->closeSession();
        $this->view("info/error", array("info" => "logout... "));  
    }



}