<?php

class Home extends Controller {

    public function index(){ //FIXME: TEST VIEW
        $this->view("home/index");
    }

    public function _index(){ //FIXME: renombrar a index
        $user = $this->model("User");
        if($user->isLoggedIn())
        {
            $priv = $user->getLoggedUserPrivilege();
            $id = $user->getLoggedUser()["id"];
            switch($priv)
            {
                case User::USER_PRIVILEGE_STUDENT:
                {
                    $plus_model = $this->model("Plus");
                    $data = array();
                    $data["point_table"] = $plus_model->getFormatedNonExpiredPlusByUser($id);
                    $this->view("student/index", $data);
                    break;
                }                   
                case User::USER_PRIVILEGE_TEACHER:
                {
                    $this->view("home/index");
                    break; 
                }
                case User::USER_PRIVILEGE_ADMINISTATOR:
                {
                    $this->view("home/index");
                    break; 
                }
            }
        }
        else
        {
            //$this->view("info/error", ["info" => "No logeado"]);
            $this->view("home/login", ["info" => "No logeado"]);

        }
        //$this->view('home/index', $test_result);
    }
}
