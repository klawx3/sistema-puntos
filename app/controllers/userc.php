<?php
class Userc extends Controller{
    

    public function index(){
        $this->view("student/add");
    }

    public function createUser()
    {
        $email = $this->post("email");        
        $nickname = $this->post("nickname");
        $fullname = $this->post("fullname");
        $passwd = $this->post("passwd");


        $user_model = $this->model("User");
        if($user_model->isUser($email,$nickname)) // usuario existe
        {
            $this->view("info/index",["info" => "Usuario ya existe", "url" => "/user"]);
        }
        else
        {
            $created = $user_model->createUser($email, $nickname, $fullname, $passwd, $user_model::USER_PRIVILEGE_STUDENT); // creando pro defecto estudiante
            if($created)
            {
                $this->view("info/index", ["info" => "Usuario creado con exito","url" => "/user"]);
            }
            else
            {
                $this->view("info/error", ["info" => "error interno del servidor al crear usuario","url" => "/user"]);
            }
        }
    }


}

?>