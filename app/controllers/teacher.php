<?php

class Teacher extends Controller 
{
    private const LIMIT_HISTORICAL_TRANSACTIONS = 25;

    public function index()
    {
        $user_model = $this->model("User"); 
        if($user_model->validateLoggedTeacher())
        {
            $transaction_model = $this->model("Transaction");
            $ht = $transaction_model->showAllHistoricTranscations(self::LIMIT_HISTORICAL_TRANSACTIONS);
            $this->view("home/teacher",["transactions" => $ht]);
        }
        else
        {
            $this->view("info/error",["error" => "no athorized"]);
        }
    }

    public function assignPoint()
    {
        $userId = $this->get("userId"); $classId = $this->get("classId"); $quantity = $this->get("quantity");  
        $expiration_days = $this->get("expirationDays"); $reciverUserId = $this->get("reciverUserId");

        $user_model = $this->model("User");
        if($user_model->validateLoggedTeacher())
        {
            if($userId && $classId && $quantity && $reciverUserId) // validar la petición POST
            { 
                $plus_model = $this->model("Plus");
                $transaction_model = $this->model("Transaction");
                $plus_id = $plus_model->getGeneratedPlus($userId, $classId , $quantity, $expiration_days);
                $transaction_model->generateTransaction($userId,$reciverUserId,$plus_id);
                $this->view("info/index", ["info" => "Transacción creada"]);
            }
            else
            {
                $this->view("info/error", ["info" => "not setted GET vars"]);
            }
        }
        else
        {
            $this->view("info/error",["info" => "non validated logged teacher or admin"]);
        }
    }

    public function usePoint($pointId = null)
    {
        $plus_model = $this->model("Plus");
        $user_model = $this->model("User");
        if($user_model->validateLoggedTeacher())
        {
            $plus_model->expirePoint($pointId);
            $this->view("info/index",["info" => "punto expirado"]);

        }
        else
        {
            $this->view("info/error",["info" => "non validated logged teacher or admin"]);
        }
    }
}

?>