<?php

class App {

    protected $controller = 'home';
    protected $method = 'index'; // metodo default
    protected $params = [];

    public function __construct(){
        
        //Controlador
        $url = $this->parseUrl();
        if(file_exists('../app/controllers/' . $url[0] . '.php')){
            $this->controller = $url[0];
            unset($url[0]);
        }

        require_once '../app/controllers/' . $this->controller . '.php';

        $this->controller = new $this->controller;

        //Metodo controlador
        if(isset($url[1])){
            if(method_exists($this->controller,$url[1])){
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        //datos
        $this->params = $url ? array_values($url) : []; // mejor visualización de los indices
        
        call_user_func_array([$this->controller,$this->method],$this->params);

    }

    protected function parseUrl(){
        if(isset($_GET['url'])){
            return explode('/',filter_var(rtrim($_GET['url'],'/'), FILTER_SANITIZE_URL));
        }
    }
}