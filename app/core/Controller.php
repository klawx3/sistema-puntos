<?php
class Controller extends DefaultApp {

    protected function post($var){
        return isset($_POST[$var]) ? $_POST[$var] : NULL;
    }

    protected function get($var){
        return isset($_GET[$var]) ? $_GET[$var] : NULL;
    }

    protected function model($name){
        if(isset($name)){
            if(file_exists('../app/models/' . $name . '.php')){
                require_once '../app/models/' . $name . '.php';
                return new $name;
            }
        }
    }

    protected function view($view, $data = []){
        if(isset($view)){
            if(file_exists('../app/views/' . $view . '.php')){
                require_once '../app/views/' . $view . '.php';
            }
        }
    }

    protected function redirect($site){
        header("Location: $site");
    }

}