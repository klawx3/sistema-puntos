<?php
class Database implements IConnectInfo {

    private static $server =    IConnectInfo::HOST;
    private static $currentDB = IConnectInfo::DBNAME;
    private static $user =      IConnectInfo::UNAME;
    private static $pass =      IConnectInfo::PW;

    private $con =      NULL;

    public function __construct(){
        $this->con = new mysqli(self::$server, self::$user, self::$pass, self::$currentDB);

        if($this->con->connect_error){
            die("Error: " . $this->conn->connect_error);
        }
    }

    public function execute($sql, $assoc = true) {
        if($this->con){
            if($r = $this->con->query($sql)){
                return $r->fetch_all($assoc ? MYSQLI_ASSOC: MYSQLI_NUM );
            }
        }
    }

    public function execute_nr($sql){
        if($this->con)
            return $this->con->query($sql);
        
    }

    
}

?>