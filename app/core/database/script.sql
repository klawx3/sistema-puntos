DROP DATABASE IF EXISTS psys ;
CREATE DATABASE psys;
USE psys;

--TABLAS
CREATE TABLE privilege (
    id INT AUTO_INCREMENT,
    description TEXT,
    level INT,

    PRIMARY KEY (id)
);

CREATE TABLE class (
    id INT AUTO_INCREMENT,
    name VARCHAR(100),
    
    PRIMARY KEY (id),
    UNIQUE (name)
);

CREATE TABLE user ( 
    id INT AUTO_INCREMENT, -- TODO: generar con uuid
    fullname VARCHAR(100),
    nickname VARCHAR(100),
    email VARCHAR(100),
    passwd VARCHAR(128), -- SHA2 512
    register_date DATE,
    privilege_id_fk INT,
    
    UNIQUE(nickname),
    UNIQUE(email),
    PRIMARY KEY (id),
    FOREIGN KEY (privilege_id_fk) REFERENCES privilege(id)
); -- SELECT * FROM user;

CREATE TABLE plus (
    id INT AUTO_INCREMENT,
    user_generated_id_fk INT,
    destiny_class_id_fk INT,
    generated_date DATETIME DEFAULT NOW(),
    expiration_date DATETIME,
    used_date DATETIME,
    quantity FLOAT,
    used BIT DEFAULT 0,

    PRIMARY KEY (id),
    FOREIGN KEY (destiny_class_id_fk) REFERENCES class(id)
);

CREATE TABLE transaction (
    id INT AUTO_INCREMENT,
    user_giver_id_fk INT, 
    user_reciver_id_fk INT, 
    transc_date DATETIME,
    plus_id_fk INT,
    move_count INT,
    active BIT,

    PRIMARY KEY (id),
    FOREIGN KEY (user_giver_id_fk) REFERENCES user(id),
    FOREIGN KEY (user_reciver_id_fk) REFERENCES user(id),
    FOREIGN KEY (plus_id_fk) REFERENCES plus(id)
);

--INSERT
INSERT INTO privilege(description,level) VALUES ('Administrator',0),
                                                ('Teacher'      ,1),
                                                ('Student'      ,2);

INSERT INTO user VALUES (NULL,'Cristian Estay','klawx3','klawx3@gmail.com',SHA2('123456',512),
                         NOW(),(SELECT id FROM privilege WHERE level = 0));

INSERT INTO user VALUES (NULL,'Alumno 1','alum1','alumno1@gmail.com',SHA2('123456',512),
                         NOW(),(SELECT id FROM privilege WHERE level = 2));

INSERT INTO class VALUES    (NULL,'Programación Web'),
                            (NULL,'Python'),
                            (NULL,'Taller de base de datos');
--QUERY


--most active points user
SELECT user_id AS 'user_id',user_email AS 'user_email',COUNT(*) as 'active_plus_quantity' 
FROM active_plus
GROUP BY user_id;

--user view non expired active_plus
SELECT (SELECT class.name FROM class WHERE id = class_id) AS 'class_name',
    plus_quantity,
    plus_expiration_date
FROM active_plus
WHERE plus_expiration_date >= NOW()
    AND user_id = 2;

--non expired active_plus
SELECT * FROM active_plus
WHERE plus_expiration_date >= NOW();

--expired active_plus
SELECT * FROM active_plus
WHERE plus_expiration_date < NOW();

--days left to plus active expiration TODO:add hour,minute,second
SELECT plus_id,DATEDIFF(plus_expiration_date,NOW()) AS 'days_left'
FROM active_plus

--x2 by user id
SELECT plus_id,DATEDIFF(plus_expiration_date,NOW()) AS 'days_left'
FROM active_plus
WHERE user_id = 1;


--BUSSINES LOGIC
--show all users transactions

CREATE VIEW all_user_transactions AS -- DROP VIEW all_user_transactions
(SELECT transaction.id AS 'transaction_id', giver_user.id AS 'giver_user_id',giver_user.email AS 'giver_user_email',
    reciver_user.id AS 'reciver_user_id',reciver_user.email AS 'reciver_user_email',
    class.id AS 'class_id',class.name AS 'class_name',
    plus.id AS 'plus_id',plus.quantity as 'plus_quantity',
    transaction.transc_date AS 'transaction_date', transaction.active  AS 'active_transaction',
    transaction.move_count AS 'move_count'
FROM transaction
    INNER JOIN user reciver_user ON transaction.user_reciver_id_fk = reciver_user.id
    INNER JOIN user giver_user ON transaction.user_giver_id_fk = giver_user.id
    INNER JOIN plus ON transaction.plus_id_fk = plus.id
    INNER JOIN class ON class.id = plus.destiny_class_id_fk
ORDER BY transaction_date ASC);
-- SELECT * FROM all_user_transactions;


CREATE VIEW active_plus AS /*DROP VIEW active_point*/
(SELECT user.id AS 'user_id',user.email as 'user_email',
    class.id AS 'class_id',class.name AS 'class_name',
    plus.id AS 'plus_id',plus.quantity as 'plus_quantity',
    plus.expiration_date AS 'plus_expiration_date'
FROM transaction
    INNER JOIN user ON transaction.user_reciver_id_fk = user.id
    INNER JOIN plus ON transaction.plus_id_fk = plus.id
    INNER JOIN class ON class.id = plus.destiny_class_id_fk
WHERE 
    transaction.active = 1);
-- SELECT * FROM active_plus;




DELIMITER // 
CREATE FUNCTION generate_plus(_user_id INT,_class_id INT,_expiration_days INT, _quantity FLOAT) RETURNS INT
BEGIN     
    DECLARE is_admin BIT;
    DECLARE new_date DATE;
    SET is_admin = (SELECT COUNT(*) 
                    FROM user 
                    INNER JOIN privilege ON user.privilege_id_fk = privilege.id 
                    WHERE user.id = _user_id AND (privilege.level = 0 OR privilege.level = 1) );    
    IF is_admin THEN        
        SET new_date = (SELECT DATE_ADD(NOW(),INTERVAL _expiration_days DAY));
        INSERT INTO plus VALUES(NULL,_user_id,_class_id,NOW(),new_date,NULL,_quantity,0);
        RETURN LAST_INSERT_ID();
    ELSE
        RETURN NULL;
    END IF;
    
END //
DELIMITER ;
-- SELECT generate_plus(1, 1, 35, 0.5) AS 'plus_id';


DELIMITER //
CREATE PROCEDURE generate_transaction(IN _giver_user_id INT,IN _reciver_user_id INT,IN _plus_id INT)
BEGIN
    
    DECLARE last_plus_id INT;
    DECLARE giver_user_exist BIT;
    DECLARE reciver_user_exist BIT;
    DECLARE class_exist BIT;
    DECLARE plus_exist BIT;
    DECLARE is_admin BIT;
    DECLARE active_transaction_id INT DEFAULT NULL;
    DECLARE old_move_count INT;
    DECLARE user_id_plus_owner INT;

    SET giver_user_exist = (SELECT COUNT(*) FROM user WHERE id = _giver_user_id);
    SET reciver_user_exist = (SELECT COUNT(*) FROM user WHERE id = _reciver_user_id);
    SET plus_exist = (SELECT COUNT(*) FROM plus WHERE id = _plus_id);
    SET is_admin = (SELECT COUNT(*) 
                    FROM user 
                    INNER JOIN privilege ON user.privilege_id_fk = privilege.id 
                    WHERE user.id = _giver_user_id AND (privilege.level = 0 OR privilege.level = 1) ); 

    IF giver_user_exist AND reciver_user_exist AND plus_exist THEN      
        SET active_transaction_id = (SELECT id FROM transaction WHERE plus_id_fk = _plus_id AND active = 1);
        
        IF active_transaction_id IS NOT NULL THEN 
            SET user_id_plus_owner = (SELECT user_reciver_id_fk FROM transaction WHERE id = active_transaction_id);
            IF _giver_user_id = user_id_plus_owner THEN                
                SET old_move_count = (SELECT move_count FROM transaction WHERE id = active_transaction_id);
                INSERT INTO transaction VALUES(NULL,_giver_user_id,_reciver_user_id,NOW(),_plus_id,(old_move_count + 1),1);
                UPDATE transaction SET active = 0 WHERE id = active_transaction_id;
            ELSE
                SELECT 'user has no permision to do this transaction, is not the owner to do this transaction' AS 'Error';
            END IF;
        ELSE /*FIXME: verificar si el punto existe de antemano (siendo admin) */
            IF is_admin THEN 
                INSERT INTO transaction VALUES(NULL,_giver_user_id,_reciver_user_id,NOW(),_plus_id,0,1);
            ELSE
                SELECT 'user has no permision to do this transaction, is not an admin to do this transaction' AS 'Error';
            END IF;
        END IF;
    ELSE
        SELECT CONCAT   ('giver_user_exist:',giver_user_exist,
                        ' reciver_user_exist:',reciver_user_exist,
                        ' plus_exist:',plus_exist) AS 'Error';
    END IF;
END //
DELIMITER ; -- DROP PROCEDURE generate_transaction;

-- CALL generate_transaction(1,2,1);
