<?php

class DefaultApp {

    protected static $database;

    protected function db(){ // simula patron singleton
        if(!self::$database){
            include_once '../app/core/database/IConnectInfo.php';
            include_once '../app/core/database/Database.php';            
            self::$database = new Database;
        }
        return self::$database;
    }
}