<?php

class User extends Model {
    
    public static $USER_SESSION_PARAM = "user_session";

    const USER_PRIVILEGE_ADMINISTATOR = 0;
    const USER_PRIVILEGE_TEACHER = 1;
    const USER_PRIVILEGE_STUDENT = 2;

    private $db;

    public function __construct()
    {
        $this->db = $this->db();
    }

    public function doLogin($userObj)
    {
        $_SESSION[self::$USER_SESSION_PARAM] = $userObj;        
    }

    public function startSession()
    {
        if(session_status() == PHP_SESSION_NONE)
            session_start();
    }

    public function closeSession()
    {
        $this->startSession();
        session_destroy();            
    }

    public function isLoggedIn()
    {
        $this->startSession();
        return isset($_SESSION[self::$USER_SESSION_PARAM]);
    }

    public function validateLoggedTeacher()
    {
        $this->startSession();
        if($this->isLoggedIn())
        {
            $priv = $this->getLoggedUserPrivilege();
            if($priv == User::USER_PRIVILEGE_TEACHER || $priv == User::USER_PRIVILEGE_ADMINISTATOR)
            {
                return true;
            }
        }
        return false;
    }


    public function createUser($email,$nickname,$fullname,$passwd,$privilege)
    {
        if($privilege != User::USER_PRIVILEGE_ADMINISTATOR || $privilege != User::USER_PRIVILEGE_TEACHER || $privilege != User::USER_PRIVILEGE_STUDENT)
        { 
            $sql = "INSERT INTO user VALUES (NULL,'$fullname','$nickname','$email',SHA2('$passwd',512),
                                             NOW(),(SELECT id FROM privilege WHERE level = $privilege))";
            $this->db->execute_nr($sql);
            return true;
        }
        return false;
        
    }
    
    public function getAllUsers()
    {
        return $this->db->execute("SELECT * FROM user");
    }

    public function isUser($email,$nickname)
    {
        $sql = "SELECT COUNT(*) AS 'n'
                FROM user
                WHERE email = '$email' OR nickname = '$nickname'";
        $rs = $this->db->execute($sql);
        return $rs[0]["n"] == "1";
    }

    public function isUserValid($email,$passwd)
    {
        $sql = "SELECT COUNT(*) AS 'n' 
                FROM user 
                WHERE email = '$email' AND passwd = SHA2('$passwd',512)";
        $rs = $this->db->execute($sql); 
        return $rs[0]["n"] == "1";
    }

    public function getLoggedUser()
    {
        return $_SESSION[self::$USER_SESSION_PARAM];
    }

    public function getUserById($id)
    {
        return $this->db->execute("SELECT * FROM user WHERE id = $id");
    }

    public function getUserByEmail($email)
    {
        $sql = "SELECT * FROM user WHERE email = '$email'";
        return $this->db->execute($sql)[0];
    }

    public function getLoggedUserPrivilege()
    {
        if(self::isLoggedIn()){
            $user = $_SESSION[self::$USER_SESSION_PARAM];            
            return self::getUserPrivilegeByEmail($user["email"]);
        }
    }

    public function getUserPrivilegeByEmail($email)
    {
        $sql = "SELECT privilege.level FROM privilege
                INNER JOIN user ON user.privilege_id_fk = privilege.id
                WHERE user.email = '$email'";
        $r = $this->db->execute($sql)[0];
        switch($r["level"]){
            case "0":
                return self::USER_PRIVILEGE_ADMINISTATOR;
            case "1":
                return self::USER_PRIVILEGE_TEACHER;
            case "2":
                return self::USER_PRIVILEGE_STUDENT;
        }
    }
}
