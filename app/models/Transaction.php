<?php

class Transaction extends Model {

    private $db = null;
    
    public function __construct()
    {
        $this->db = $this->db();
    }

    public function generateTransaction($giver_uid,$reciver_uid,$plus_id)
    {
        $sql = "CALL generate_transaction($giver_uid,$reciver_uid,$plus_id)";
        return $this->db->execute_nr($sql); // FIXME: genera la transacción sin retroalimentación
    }

    public function showAllTransactionsByGiverUserId($user_id)
    {
        return $this->db->execute("SELECT * FROM all_user_transactions WHERE giver_user_id = $user_id");
    }

    public function showAllTransactionsByReciverUserId($user_id)
    {
        return $this->db->execute("SELECT * FROM all_user_transactions WHERE reciver_user_id = $user_id");
    }

    public function showAllHistoricTranscations($limit = null)
    {
        $sql = "SELECT * FROM all_user_transactions";
        if($limit != NULL) 
        {
            $sql .= " LIMIT $limit";
        }     
        echo $sql;              
        return $this->db->execute($sql);
    }
}