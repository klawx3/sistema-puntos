<?php
class Plus extends Model{

    private $db;
    
    public function __construct(){
        $this->db = $this->db();
    }

    public function expirePoint($plusId) // TODO: GEN function
    {
        $sql = "UPDATE transaction SET transaction.active = 0 WHERE transaction.plus_id_fk = $plusId";
        return $this->db->execute_nr($sql);
    }

    public function getGeneratedPlus($userId, $classId ,$quantity,$expiration_days )
    {
        $sql = "SELECT generate_plus ($userId,$classId,$expiration_days,$quantity) AS 'plus_id'";
        $result = $this->db->execute($sql);
        $id = $result[0]["plus_id"];
        return $id;
    }

    public function getFormatedNonExpiredPlusByUser($user_id){
        $sql = "SELECT plus_id,
                    (SELECT class.name FROM class WHERE id = class_id) AS 'class_name',
                    plus_quantity,
                    plus_expiration_date
                FROM active_plus
                WHERE plus_expiration_date >= NOW()
                    AND user_id = $user_id";
        return $this->db->execute($sql);
    }

    public function getNonExpiredActivePlus(){
        return $this->db->execute("SELECT * FROM active_plus 
                                WHERE plus_expiration_date >= NOW()");
    }

    public function getExpiredActivePlus(){
        return $this->db->execute("SELECT * FROM active_plus
                                WHERE plus_expiration_date < NOW()");
    }

    public function getDaysLeftToAllActivePlusExpiration(){
        return $this->db->execute("SELECT id,DATEDIFF(expiration_date,NOW()) AS 'days_left'
                                FROM plus");
    }

    public function daysLeftToAllActivePlusExpiration(){
        return $this->db->execute("SELECT plus_id,DATEDIFF(plus_expiration_date,NOW()) AS 'days_left'
                                FROM active_plus");
    }

    public function daysLeftToAllActivePlusExpirationByUserId($user_id){
        return $this->db->execute("SELECT plus_id,DATEDIFF(plus_expiration_date,NOW()) AS 'days_left'
                                FROM active_plus
                                WHERE user_id = $user_id");
    }
}